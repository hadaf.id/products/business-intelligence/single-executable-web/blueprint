# Blueprint

## Requirements
- Single executable web
    - Web server and web front end are bundled into single executable file
    - Could be deployed directly on:
        - Desktop
            - Windows
            - Linux
            - Mac
        - Web server
        - Docker
- Graphs-based ETL pipeline
    - Inspired by Node-RED, Apache Airflow, Kettle
- Advanced visualization
    - Leveraging rich JavaScript visualization technologies such as
        - Canvas
        - Web GL
- Which one is your focus is ?
    - Little to medium data processing for common people
    - Big data processing for data profession

## Milestones
1. Data visualization
    - UX / UI
        - Basic UX / UI
    - Basic data source connection
        - Google Sheets
        - CSV
        - Excel
        - PostgreSQL
    - Unified data representation       
    - Simple chart generation
    - Custom real-time dashboard
        - Shareable URL
2. ETL pipeline

## Technologies
- Backend choices
    - Python
        - A lot of data-related libraries
        - The dynamic nature of this language makes it easier to do complex dynamic data processing compared to Rust and Golang
    - JavaScript
        - Many data-related libraries
        - The dynamic nature of this language makes it easier to do complex dynamic data processing compared to Rust and Golang
    - Golang
        - Higher processing speed and lower resource usage compared to Python and JavaScript
        - Simpler syntax compared with Rust
        - Used by many CNCF projects such as Docker, Kubernetes
        - Seems to be easier to create multi-OS executable compared with the rest
        - Project sizes much smaller than Rust-based project
        - Compiled much faster than Rust
    - Rust
        - Fastest processing speed compared with the rest